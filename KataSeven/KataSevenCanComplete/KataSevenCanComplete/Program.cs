﻿using System;

namespace KataSevenCanComplete
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("CanComlete: " + CanComplete("b","b"));
            Console.WriteLine("CanComlete: " + CanComplete("butl", "beautiful"));
            Console.WriteLine("CanComlete: " + CanComplete("butlz", "beautiful"));
            Console.WriteLine("CanComlete: " + CanComplete("tulb", "beautiful"));
            Console.WriteLine("CanComlete: " + CanComplete("bbutl", "beautiful"));
            Console.WriteLine("CanComlete: " + CanComplete("etfl", "beautiful"));
        }
        static bool CanComplete(string str, string word) 
        {
            int startLether = 0;
            for (int i = 0; i < word.Length; i++)
            {
                if(str[startLether].Equals(word[i]))
                    startLether++;
            }
            if(startLether.Equals(str.Length))
                return true;
            else
                return false; 
        }
    }
}
