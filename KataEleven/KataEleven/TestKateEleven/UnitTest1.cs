using KataEleven;
using System;
using Xunit;

namespace TestKateEleven
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //Arrange
            string message = "Invalid";
            Password p = new Password();
            //Act
            string actMessage = p.PasswordTester("stonk");
            //Assert
            Assert.Equal(message, actMessage);
        }
        [Fact]
        public void Test2()
        {
            //Arrange
            string message = "Invalid";
            Password p = new Password();
            //Act
            string actMessage = p.PasswordTester("pass word");
            //Assert
            Assert.Equal(message, actMessage);
        }
        [Fact]
        public void Test3()
        {
            //Arrange
            string message = "Weak";
            Password p = new Password();
            //Act
            string actMessage = p.PasswordTester("password");
            //Assert
            Assert.Equal(message, actMessage);
        }
        [Fact]
        public void Test4()
        {
            //Arrange
            string message = "Weak";
            Password p = new Password();
            //Act
            string actMessage = p.PasswordTester("11081992");
            //Assert
            Assert.Equal(message, actMessage);
        }
        [Fact]
        public void Test5()
        {
            //Arrange
            string message = "Moderate";
            Password p = new Password();
            //Act
            string actMessage = p.PasswordTester("mySecurePass12");
            //Assert
            Assert.Equal(message, actMessage);
        }
        [Fact]
        public void Test6()
        {
            //Arrange
            string message = "Moderate";
            Password p = new Password();
            //Act
            string actMessage = p.PasswordTester("!@!pass1");
            //Assert
            Assert.Equal(message, actMessage);
        }
        [Fact]
        public void Test7()
        {
            //Arrange
            string message = "Strong";
            Password p = new Password();
            //Act
            string actMessage = p.PasswordTester("@S3cur1ty");
            //Assert
            Assert.Equal(message, actMessage);
        }

    }
}
