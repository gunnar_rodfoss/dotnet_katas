﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KataEleven
{
    public class Password
    {
        public string PasswordTester(string password)
        {
            int matchCasses = 0;
            int test = 0;
            if (password.Length < 6 || password.Contains(" "))
                return "Invalid";
            for (int i = 0; i < password.Length; i++)
            {
                if (char.IsLower(password[i]))
                    test++;
            }
            if (test > 0)
                matchCasses++;
            test = 0;
            for (int i = 0; i < password.Length; i++)
            {
                if (char.IsUpper(password[i]))
                    test++;
            }
            if (test > 0)
                matchCasses++;
            test = 0;
            for (int i = 0; i < password.Length; i++)
            {
                if (char.IsSymbol(password[i]))
                    test++;
            }
            if (test > 0)
                matchCasses++;
            test = 0;
            for (int i = 0; i < password.Length; i++)
            {
                if (!char.IsLetterOrDigit(password[i]))
                    test++;
            }
            if (test > 0)
                matchCasses++;
            test = 0;
            for (int i = 0; i < password.Length; i++)
            {
                if (char.IsDigit(password[i]))
                    test++;
            }
            if (test > 0)
                matchCasses++;
            test = 0;
            //Console.WriteLine("matchCasses " + matchCasses);
            if (password.Length >= 8)
                matchCasses++;
            //Console.WriteLine("matchCasses " + matchCasses);
            if (matchCasses > 2 && matchCasses <= 4)
                return "Moderate";
            if (matchCasses <= 2)
                return "Weak";
            return "Strong";
        }
    }
}
