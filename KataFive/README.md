1

## KATA: PascalCase converter
The goal of this kata is to make use of String manipulation 
techniques in C#. You are given a sentence and have to convert it
to PascalCase, i.e. “pineapple on pizza” -> “PineappleOnPizza”. 
Each word needs to be capitalized and the spaces removed. 
You do not need to ask for user input, rather simply take a 
provided sentence and do the conversion. The sentence given is 
“The quick brown fox jumped over the lazy dog”. Convert this.
# Some things to think about: 
- How will you handle irregular capitalization? i.e. jUmpED
- How will you handle multiple whitespaces, i.e the quick     
brown
# (Optional) How will you handle punctuation, i.e. the quick, brown! 
fox.
- Remove punctuation (.,!?;:)
For these extra things to think about and the you can use the 
following sentence:
“The qUick!  bRoWn fox    jumped OVER the    lazy dog”
For the optional punctuation removal:
“The qUick!  bRoWn fox    jumped, OVER the    lazy. dog”