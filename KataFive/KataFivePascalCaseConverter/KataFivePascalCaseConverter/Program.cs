﻿using System;

namespace KataFivePascalCaseConverter
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(PascalCaseConverter("is krem"));
            Console.WriteLine(PascalCaseConverter("pineapple on pizza"));
            Console.WriteLine(PascalCaseConverter("The qUick!  bRoWn fox    jumped OVER the    lazy dog"));
            Console.WriteLine(PascalCaseConverter("The qUick!  bRoWn fox    jumped, OVER the    lazy. dog"));
        }

        static string PascalCaseConverter(string word)
        { 
            string[] split = { " ", ".", ",", "!", "?" };
            string[] wordArr = word.Split( split, StringSplitOptions.RemoveEmptyEntries);
            string newWord = "";
 
            for (int i = 0; i < wordArr.Length; i++)
            {
                char[] a = wordArr[i].ToCharArray();
                a[0] = char.ToUpper(a[0]);
                newWord += "" + new string(a);

            }

            return newWord;
            
        }
    }
}
