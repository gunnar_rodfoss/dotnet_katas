## KATA: snake_case and camelCase
---
Create two functions ToCamelCase() and ToSnakeCase() that 
each take a single string and convert it into either camelCase or 
snake_case.
---
## Examples
- 1. ToCamelCase("hello_edabit") ➞ "helloEdabit"
- 2. ToSnakeCase("helloEdabit") ➞ "hello_edabit"
- 3. ToCamelCase("is_modal_open") ➞ "isModalOpen"
- 4. ToSnakeCase("getColor") ➞ "get_color"
# Notes
---
Test input will always be appropriately formatted as either 
camelCase or snake_case, depending on the function being 
called.
---