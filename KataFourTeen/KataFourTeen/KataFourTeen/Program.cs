﻿using System;

namespace KataFourTeen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("helloEdabit: " + ToCamelCase("hello_edabit"));
            Console.WriteLine("isModalOpen: " + ToCamelCase("is_modal_open"));

            Console.WriteLine("hello_edabit: " + ToSnakeCase("helloEdabit"));
            Console.WriteLine("is_modal_open: " + ToSnakeCase("isModalOpen"));
        }
        static string ToCamelCase(string str)
        {

            for(int i = 0; i < str.Length; i++)
            {
                if (str[i] == '_')
                {
                    char temp = str[i + 1];
                    str = str.Remove(i, 2);
                    str = str.Insert(i, temp.ToString().ToUpper());
                }
            }
            return str;
        }
        static string ToSnakeCase(string str)
        {

            for (int i = 0; i < str.Length; i++)
            {
                if (char.IsUpper(str[i]))
                {
                    char temp = str[i];
                    str = str.Remove(i, 1);
                    str = str.Insert(i, '_'.ToString());
                    str = str.Insert(i+1, temp.ToString().ToLower());
                }
            }
            return str;
        }
    }
}
