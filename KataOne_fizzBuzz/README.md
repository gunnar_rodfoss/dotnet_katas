## KATA: Fizzbuzz
    Starting of very simple just to get used to the mechanics of a 
    kata.
# You are required to make a method called ConvertFizzBuzz which takes in a single integer value. The method will return either: 
    “Fizz”, “Buzz”, “FizzBuzz”, or the number (as a string).
# The logic for fizzbuzz goes as follows:
    If the number is divisible by 3, return Fizz.
    If the number is divisible by 5, return Buzz.
    If the number is divisible by both 3 and 5, return FizzBuzz.
    Else, return the number as a string.
    Loop through the numbers 1-100 and print the converted fizzbuzz
    values to the console.
    i.e. 1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, 
    FizzBuzz, 16, ... 