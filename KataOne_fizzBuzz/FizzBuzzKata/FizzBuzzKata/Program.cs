﻿using System;

namespace FizzBuzzKata
{
    internal class Program
    {
        static void Main(string[] args)
        {
            for(int i = 0; i <= 100; i++)
            {
                Console.WriteLine(ConvertFizzBuzz(i));
            }
            
            
        }
        public static string ConvertFizzBuzz(int num) {

            if (num % 3 == 0 && num % 5 != 0)
                return "Fizz";
            if (num % 3 != 0 && num % 5 == 0)
                return "Buzz";
            if (num % 3 == 0 && num % 5 == 0)
                return "FizzBuzz";


            return $"{num}";

        }
    }
}
