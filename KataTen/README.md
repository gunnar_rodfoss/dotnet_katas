## KATA: Roman numeral converter
Write a function to convert from Roman Numerals to decimal 
numbers. You don’t need to worry about validating the input.
Roman numerals, the numeral system of ancient Rome, uses 
combinations of letters from the Latin alphabet to signify values. 
They are based on seven symbols: 
https://www.rapidtables.com/convert/number/roman-numerals-
converter.html 
# NOTE: You can use this converter to create test data for yourself.

---
SymbolValue
I 1
V 5
X 10
L 50
C 100
D 500
M 1000
---

Numbers are formed by combining symbols together and adding 
the values. Generally, symbols are placed in order of value, 
starting with the largest values. When smaller values precede 
larger values, the smaller values are subtracted from the larger 
values, and the result is added to the total.
# Example:
Roman Number Computation Value
MMVI 1000 + 1000 + 5 + 1 2006
MCMXLIV 1000 + (1000 - 100) + (50 - 10) + (5 - 1)1944