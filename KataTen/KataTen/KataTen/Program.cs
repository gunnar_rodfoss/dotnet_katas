﻿using System;
using System.Collections.Generic;

namespace KataTen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("2006: " + RomanNumeralConverter("MMVI"));
            Console.WriteLine("1944: " + RomanNumeralConverter("MCMXLIV"));
            Console.WriteLine("11: " + RomanNumeralConverter("IXII"));
        }
        public static int RomanNumeralConverter(string number)
        {
            string[] RomanNum = {"I","V","X","L","C","D","M" };
            int[] RomanDesimalValue = { 1, 5, 10, 50, 100, 500, 1000 };
            List<int> desimalList = new List<int>();
            int desimalValue = 0;
            for(int i = 0; i < number.Length; i++)
            {
                for(int j = 0; j < RomanNum.Length; j++)
                {
                    if (number[i].ToString() == RomanNum[j])
                        desimalList.Add(RomanDesimalValue[j]);    
                }
            }
            for (int k = 0; k < desimalList.Count; k++)
            {
                if(k < desimalList.Count -1)
                {
                    if (desimalList[k] < desimalList[k + 1])
                    {
                        desimalValue += desimalList[k + 1] - desimalList[k];
                        desimalList.RemoveAt(k);
                    }
                    else
                        desimalValue += desimalList[k];
                }
                else
                    desimalValue += desimalList[k];
            }
                return desimalValue;
        }
    }
}
