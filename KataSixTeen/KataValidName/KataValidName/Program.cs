﻿using System;

namespace KataValidName
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("True: " + ValidName("H. Wells"));
            Console.WriteLine("True: " + ValidName("H. G. Wells"));
            Console.WriteLine("True: " + ValidName("Herbert G. Wells"));
            Console.WriteLine("False: " + ValidName("herbert G. Wells"));
            Console.WriteLine("False: " + ValidName("H. George Wells"));
            Console.WriteLine("False: " + ValidName("Herb. Wells"));
            Console.WriteLine("False: " + ValidName("Herber"));
            Console.WriteLine("False: " + ValidName("h. Wells"));
            Console.WriteLine("False: " + ValidName("H Wells"));
            Console.WriteLine("False: " + ValidName("H. George Wells"));
            Console.WriteLine("False: " + ValidName("H. George W."));
            Console.WriteLine("False: " + ValidName("Herb. George Wells"));


        }
        static bool ValidName(string name)
        {
            string[] nameArr = name.Split(' ');
            if (nameArr.Length < 2)
                return false;
            //foreach (string s in nameArr) { Console.WriteLine(s); }
            if(nameArr.Length == 3)
            {
                if (nameArr[0].Length == 1 || nameArr[1].Length == 1 || nameArr[2].Length == 1)
                    return false;
                // Cannot have: initial first name + word middle name
                if (nameArr[0].Length == 2 && nameArr[1].Length > 2)
                    return false;
                if((nameArr[0].Length > 2 && (nameArr[0].Contains('.') || !Char.IsUpper(nameArr[0][0]) )) || 
                    (nameArr[1].Length > 2 && (nameArr[1].Contains('.') || !Char.IsUpper(nameArr[1][0]) ) ) || 
                    (nameArr[2].Length > 2 && (nameArr[2].Contains('.') || !Char.IsUpper(nameArr[2][0]) ) ))
                    return false;
                if (nameArr[0].Length == 2)
                {
                    if(!Char.IsUpper(nameArr[0][0]))
                        return false;
                    if(!nameArr[0].Contains('.'))
                        return false;
                }
                if (nameArr[1].Length == 2)
                {
                    if (!Char.IsUpper(nameArr[1][0]))
                        return false;
                    if (!nameArr[1].Contains('.'))
                        return false;
                }
                if (nameArr[2].Length == 2)
                {
                    if (!Char.IsUpper(nameArr[1][0]))
                        return false;
                    if (nameArr[1].Contains('.'))
                        return false;
                }
            }
            if (nameArr.Length == 2)
            {
                if ((nameArr[0].Length > 2 && (nameArr[0].Contains('.') || !Char.IsUpper(nameArr[0][0]))) ||
                    (nameArr[1].Length > 2 && (nameArr[1].Contains('.') || !Char.IsUpper(nameArr[1][0]))))
                    return false;
                //Console.WriteLine("INSIDE");
                if (nameArr[0].Length == 1 || nameArr[1].Length == 1)
                    return false;
                if (nameArr[0].Length == 2)
                {
                    if (!Char.IsUpper(nameArr[0][0]))
                        return false;
                    if (!nameArr[0].Contains('.'))
                        return false;
                }
                if (nameArr[1].Length == 2)
                {
                    if (!Char.IsUpper(nameArr[1][0]))
                        return false;
                    if (nameArr[1].Contains('.'))
                        return false;
                }
            }
            return true;
        }
    }
}
