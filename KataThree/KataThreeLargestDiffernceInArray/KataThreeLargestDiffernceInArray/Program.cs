﻿using System;

namespace KataThreeLargestDiffernceInArray
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] numbersOne = { 2, 3, 1, 7, 9, 5, 11, 3, 5 };
            int[] numbersTwo = { 2, 3, 11, 7, 9, 5, 1, 3, 5 };
            int[] numbersThree = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            

            Console.WriteLine(LargestDiff(numbersOne));
            Console.WriteLine(LargestDiff(numbersTwo));
            Console.WriteLine(LargestDiff(numbersThree));
            

            Console.WriteLine(PairsOfMaximum(numbersOne, LargestDiff(numbersOne)));
            Console.WriteLine(PairsOfMaximum(numbersTwo, LargestDiff(numbersTwo)));
        }
        static int LargestDiff(int[] arr) {
            int diff = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i+1; j < arr.Length; j++)
                {
                    int temp = arr[j] - arr[i] ;
                    if (temp > diff)
                        diff = temp;
                }
            }
            return diff; 
        
        }
        static int PairsOfMaximum(int[] arr, int max) {
            int pairsMax = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i+1; j < arr.Length; j++)
                {
                    int temp = arr[j] + arr[i];
                    if (temp.Equals(max))
                        pairsMax++;
                }
            }
            return pairsMax;
        }   
    }
}
