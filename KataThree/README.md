
## KATA: Largest difference in Array
The goal of this kata is to make use of Array manipulation 
techniques. You are given an array containing integers. You are to
find the maximum difference between two elements. 
The smaller element must appear before (its index is less than) 
larger element in the array (can’t simply use .Min() and .Max()).
Meaning: At index 2, look for numbers from index 3 onwards that 
are larger. The largest difference is recorded. Do this for all 
elements to get the largest difference.  
# Example:
Input:
numbers = { 2, 3, 1, 7, 9, 5, 11, 3, 5 }
Output:
The maximum difference between two array elements is:
10
Explanation:
10 = 11(6) – 1(2)
# Another example:
Input:
numbers = { 2, 3, 11, 7, 9, 5, 1, 3, 5 }
Output:
9
Explanation:
9 = 11(2) – 2(0)
## Optional extra:
See how many pairs of numbers added together, in the order they
appear in (to eliminate repeats), give that same maximum.
Example (with indexes included):
3(1) + 7(3) = 10
1(2) + 9(4) = 10
7(3) + 3(7) = 10
5(5) + 5(8) = 10
Therefore, there are 4 pairs that add up to that maximum.