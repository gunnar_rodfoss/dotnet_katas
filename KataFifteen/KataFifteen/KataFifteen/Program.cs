﻿using System;

namespace KataFifteen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*Console.WriteLine("ValidRgbColour(“rgb(0,0,0)” " + ValidRgbColour("rgb(0,0,0)"));
            Console.WriteLine("ValidRgbColour(“rgb(255,255,255”) " + ValidRgbColour("rgb(255,255,255)"));
            Console.WriteLine("ValidRgbColour(“rgb(0%,50%,100%)”)) " + ValidRgbColour("rgb(0%,50%,100%)"));
            Console.WriteLine("INVALID " + ValidRgbColour("rgb(0,,0)"));
            Console.WriteLine("INVALID " + ValidRgbColour("“rgb (0,0,0)"));
            Console.WriteLine("INVALID " + ValidRgbColour("rgb(0,0,0,0)"));
            Console.WriteLine("INVALID " + ValidRgbColour("rgb(-1,0,0)"));
            Console.WriteLine("INVALID " + ValidRgbColour("rgb(255,256,255"));
            Console.WriteLine("INVALID " + ValidRgbColour("rgb(100%,100%,101%"));
            */
            //Console.WriteLine(" " + double.Parse(".45"));

            //Console.WriteLine("ValidRgbColour(“rgba(0,0,0,0)”)) " + ValidRgbColour("rgba(0,0,0,0)"));
            //Console.WriteLine("ValidRgbColour(“rgba(255,255,255,1)”)) " + ValidRgbColour("rgba(255,255,255,1)"));
            //Console.WriteLine("ValidRgbColour(“rgba(0,0,0,0.123456789)” " + ValidRgbColour("rgba(0,0,0,0.123456789)"));
            //Console.WriteLine("ValidRgbColour(“rgba(0,0,0,.8)”)) " + ValidRgbColour("rgba(0,0,0,.8)"));
            //Console.WriteLine("ValidRgbColour(“rgba( 0 , 127 , 255 , 0.1)”)) " + ValidRgbColour("rgba( 0 , 127 ,     255 , 0.1)"));
            //Console.WriteLine("INVALID " + ValidRgbColour("rgba(0,0,0)"));
            //Console.WriteLine("INVALID " + ValidRgbColour("rgba(0,0,0,-1)"));
            //Console.WriteLine("INVALID " + ValidRgbColour("rgba(0,0,0,1.1"));
        }
        static bool ValidRgbColour(string rgbStr)
        {
            if (rgbStr.Contains("rgba"))
            {
                rgbStr=rgbStr.Replace("rgba", "");
                rgbStr = rgbStr.Replace("(", "");
                rgbStr = rgbStr.Replace(")", "");
                Console.WriteLine(rgbStr);
                string[] rgbaArr = rgbStr.Split(",");
                foreach (var el in rgbaArr) { Console.WriteLine(el); }
                if(rgbaArr.Length < 4)
                {
                    return false;
                }
                if (rgbaArr[3].StartsWith('.'))
                {
                    Console.WriteLine("INSIDE: " + double.Parse("0,39"));
                    rgbaArr[3] = "0" + rgbaArr[3];
                }
                    
                //if(double.Parse(rgbaArr[3].ToString()) > 0)
                    //return false;
                    for (int i = 0; i < rgbaArr.Length-1; i++)
                {
                    if (rgbaArr[i].Equals(""))
                        return false;
                    if (int.Parse(rgbaArr[i].ToString()) > 255 || int.Parse(rgbaArr[i].ToString()) < 0)
                        return false;
                }
                return true;
            }
            if (rgbStr.Contains("rgb"))
            {
                if (rgbStr.Contains("rgb "))
                    return false;
                rgbStr = rgbStr.Replace("rgb", "");
                rgbStr = rgbStr.Replace("(", "");
                rgbStr = rgbStr.Replace(")", "");
                rgbStr = rgbStr.Replace(" ", "");
                if (rgbStr.Contains("%"))
                {
                    rgbStr = rgbStr.Replace("%", "");
                    string[] rgbaArrPro = rgbStr.Split(",");
                    for (int i = 0; i < rgbaArrPro.Length; i++)
                    {
                        if (rgbaArrPro[i].Equals(""))
                            return false;
                        if (int.Parse(rgbaArrPro[i].ToString()) > 100 || int.Parse(rgbaArrPro[i].ToString()) < 0)
                            return false;
                    }
                }
                
                Console.WriteLine(rgbStr);
                string[] rgbaArr = rgbStr.Split(",");
                //foreach (var el in rgbaArr) { Console.WriteLine(el); }
                if (rgbaArr.Length != 3)
                {
                    return false;
                }
                for (int i = 0; i < rgbaArr.Length; i++)
                {
                    if (rgbaArr[i].Equals(""))
                        return false;
                    if (int.Parse(rgbaArr[i].ToString()) > 255 || int.Parse(rgbaArr[i].ToString()) < 0)
                        return false;
                }
                return true;
            }
            return false;
        }
    }
}
