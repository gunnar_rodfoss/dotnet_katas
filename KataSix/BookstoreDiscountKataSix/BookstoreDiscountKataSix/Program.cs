﻿using System;

namespace BookstoreDiscountKataSix
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("DiscountAmount: " + BookstoreDiscount( new int[]{ 0, 1} ));
            Console.WriteLine("DiscountAmount: " + BookstoreDiscount(new int[] { 0, 2, 3 }));
            Console.WriteLine("DiscountAmount: " + BookstoreDiscount(new int[] { 0, 1, 2, 3 }));
            Console.WriteLine("DiscountAmount: " + BookstoreDiscount(new int[] { 1, 1,1 }));
            Console.WriteLine("DiscountAmount: " + BookstoreDiscount(new int[] { 1 }));
            Console.WriteLine("DiscountAmount: " + BookstoreDiscount(new int[] {  }) );

        }
        static double BookstoreDiscount(int[] arr)
        {
            if(arr.Length.Equals(1))
                return 8;
            if (arr.Length.Equals(0))
                return 0;
            double[] discounts = {1, 0.95, 0.9, 0.8};    
            int multibleBooks = 0;
            
            int discountAmount = 1;
            for(int i = 0; i < arr.Length; i++)
            {
                for(int j = 0; j < arr.Length; j++)
                {
                    if(arr[i] != arr[j])
                        multibleBooks++;
                }
                if(multibleBooks >=2 && multibleBooks < 5 )
                    discountAmount=multibleBooks;
                multibleBooks=0;
                
            }
            return 8.0*(double)discountAmount*discounts[discountAmount-1];
        }

       /*static double BookstoreDiscountOptionalExtra(int[] arr)
        {
            if (arr.Length.Equals(1))
                return 8;
            if (arr.Length.Equals(0))
                return 0;
            double[] discounts = { 1, 0.95, 0.9, 0.8 };
            int multibleBooks = 0;

            int discountAmount = 1;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (arr[i] != arr[j])
                        multibleBooks++;
                }
                if (multibleBooks >= 2 && multibleBooks < 5)
                    discountAmount = multibleBooks;
                multibleBooks = 0;

            }
            return (8.0 * (arr.Length - discountAmount) ) + (8.0 * (double)discountAmount * discounts[discountAmount - 1]);        
        }*/
    }
}
