## KATA: Freddy Fridays
Write a program which counts and displays the number of occurrences of Friday the 13th in a particular year. 
For validation purposes, use the current year as the starting year and 3000 as the maximum year.
Optional extra: Display the subset of years which have the maximum number of Friday the 13ths.
