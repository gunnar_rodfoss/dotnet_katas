## KATA: Human-readable time
Write a method, which takes a non-negative integer (seconds) as 
input and returns the time in a human-readable format 
(HH:MM:SS).
HH = hours, padded to 2 digits, range: 00 – 99
MM = minutes, padded to 2 digits, range: 00 – 59
SS = seconds, padded to 2 digits, range: 00 – 59
The maximum time never exceeds 359999 (99:59:59).
# Tests:
Assert.Equal(”00:00:00′′, TimeFormat.GetReadableTime(0));
Assert.Equal (”00:00:05′′, TimeFormat.GetReadableTime(5));
Assert.Equal (”00:01:00′′, TimeFormat.GetReadableTime(60));
Assert.Equal (”23:59:59′′, TimeFormat.GetReadableTime(86399));
Assert.Equal (”99:59:59′′, 
TimeFormat.GetReadableTime(359999));