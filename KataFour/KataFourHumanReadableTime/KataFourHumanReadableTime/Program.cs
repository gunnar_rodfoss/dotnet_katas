﻿using System;

namespace KataFourHumanReadableTime
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Time is:" + ReadableTime(0));
            Console.WriteLine("Time is:" + ReadableTime(5));
            Console.WriteLine("Time is:" + ReadableTime(60));
            Console.WriteLine("Time is:" + ReadableTime(86399));
            Console.WriteLine("Time is:" + ReadableTime(359999));
            Console.WriteLine("Time is:" + ReadableTime(3600));
            Console.WriteLine();

            Console.WriteLine("Time is from TimeSpan:" + TimeSpan.FromSeconds(0));
            Console.WriteLine("Time is from TimeSpan:" + TimeSpan.FromSeconds(5));
            Console.WriteLine("Time is from TimeSpan:" + TimeSpan.FromSeconds(60));
            Console.WriteLine("Time is from TimeSpan:" + TimeSpan.FromSeconds(86399));
            Console.WriteLine("Time is from TimeSpan:" + TimeSpan.FromSeconds(359999));
        }
        static string ReadableTime(int time) {
            int min = 0; 
            int hour = 0;
            String sMin = "";
            String sHour = "";
            String sSec = "";
            if (time >= 3600)
            {
                hour = time / 3600;
                time = time - (3600 * hour);
            }
            if(time >= 60)
            {
                min = time / 60;
                time = time - (60*min);
            }
            if (hour < 10)
                sHour = $"0{hour}";
            else
                sHour = $"{hour}";
            if (min < 10)
                sMin = $"0{min}";
            else
                sMin = $"{min}";
            if (time < 10)
                sSec = $"0{time}";
            else
                sSec = $"{time}";
            return sHour + ":" + sMin + ":" + sSec;//return $"{hour}:{min}:{time}";
        }
    }
}
