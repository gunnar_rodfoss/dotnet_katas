﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KataTwelve
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("false: " + ValidCreditCard(1234567890123456));
            Console.WriteLine("true: " + ValidCreditCard(1234567890123452));

            Console.WriteLine("false: " + ValidCreditCard(79927398714));
            Console.WriteLine("false: " + ValidCreditCard(79927398713));
            Console.WriteLine("true: " + ValidCreditCard(709092739800713));
            Console.WriteLine("false: " + ValidCreditCard(1234567890123456));

            Console.WriteLine("true: " + ValidCreditCard(5496683867445267));

            Console.WriteLine("false: " + ValidCreditCard(4508793361140566));
            Console.WriteLine("true: " + ValidCreditCard(376785877526048));
            Console.WriteLine("false: " + ValidCreditCard(36717601781975));
        }
        static bool ValidCreditCard(long cardNum)
        {
            int sum = 0;
            string cardNumStr = cardNum.ToString();
            List<int> cardNumList = new List<int>();
            for (int i = 0; i < cardNumStr.Length; i++)
            {
                cardNumList.Add(int.Parse(cardNumStr[i].ToString()) );
            }
            if (!(cardNumList.Count() >= 14 && cardNumList.Count() <= 19))
                return false;
            int checkInt = cardNumList[cardNumList.Count()-1];
            cardNumList.RemoveAt(cardNumList.Count() - 1);
            cardNumList.Reverse();
            for (int i = 0; i < cardNumList.Count(); i+=2)
            {
                  int temp = cardNumList[i] * 2;
                    if(temp >= 10)
                    {
                        string tempStr = temp.ToString();
                        temp = int.Parse(tempStr[0].ToString()) + int.Parse(tempStr[1].ToString());
                    }
                    cardNumList[i] = temp;
            }
            sum = cardNumList.Sum();
            string sumStr = sum.ToString();
            if ((10 - int.Parse(sumStr[sumStr.Length - 1].ToString()) ) == checkInt)
                return true;
            else
                return false;
        }
       
    }
}
