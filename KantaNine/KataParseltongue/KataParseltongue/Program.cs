﻿using System;

namespace KataParseltongue
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("True:"  + Parseltongue("Sshe ssselects to eat that apple."));
            Console.WriteLine("False:" + Parseltongue("She ssselects to eat that apple."));

            Console.WriteLine("False:" + Parseltongue("Beatrice samples lemonade"));

            Console.WriteLine("True:"  + Parseltongue("You ssseldom sssspeak sso boldly, ssso messmerizingly."));

            Console.WriteLine("False:" + Parseltongue("Steve likes to eat pancakes"));
            Console.WriteLine("True:"  + Parseltongue("Sssteve likess to eat pancakess"));
            Console.WriteLine("False:" + Parseltongue("Beatrice samples lemonade"));
            Console.WriteLine("True:"  + Parseltongue("Beatrice enjoysss lemonade"));
        }
        static bool Parseltongue(string sentence)
        {
            sentence = sentence.ToLower();
            string[] words = sentence.Split(' ');
            for(int i = 0; i < words.Length; i++)
            {
                if( !words[i].Contains("ss") && words[i].Contains("s"))
                    return false;
            }
            return true;
        }
    }
}
