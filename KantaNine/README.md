## Kata: Parseltongue
Hermione has come up with a precise formula for determining whether or not
a phrase was ssspoken by a parssseltongue (a reference from the Harry 
Potter universe; the language of ssserpents and those who can converse with
them).
Each word in a sssentence must contain either:
- Two or more consecutive instances of the letter "s" (i.e. must be 
together ss..), or...
- Zero instances of the letter "s" by itself. (No other “ss” in the word)
# Examplesss
IsParselTongue("Sshe ssselects to eat that apple. ") ➞ true
IsParselTongue("She ssselects to eat that apple. ") ➞ false
// "She" only contains one "s".
IsParselTongue("Beatrice samples lemonade") ➞ false
// While "samples" has 2 instances of "s", they are not together.
IsParselTongue("You ssseldom sssspeak sso boldly, ssso messmerizingly.") ➞
true
# Testsss
[Test]
[TestCase("Sshe ssselects to eat that apple.", Result=true)]
[TestCase("She ssselects to eat that apple.", Result=false)]
[TestCase("You ssseldom sssspeak sso boldly, ssso messmerizingly.", 
Result=true)]
[TestCase("Steve likes to eat pancakes", Result=false)]
[TestCase("Sssteve likess to eat pancakess", Result=true)]
[TestCase("Beatrice samples lemonade", Result=false)]
[TestCase("Beatrice enjoysss lemonade", Result=true)]