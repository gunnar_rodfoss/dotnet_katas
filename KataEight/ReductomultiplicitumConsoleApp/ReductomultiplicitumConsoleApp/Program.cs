﻿using System;

namespace ReductomultiplicitumConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("multiplicitum: " );//9, 8
            Console.WriteLine("multiplicitum 0 : " + multiplicitum(new int[] { 0 }));
            Console.WriteLine("multiplicitum 9 : " + multiplicitum(new int[] { 9 }));
            Console.WriteLine("multiplicitum 7 : " + multiplicitum(new int[] { 9, 8 }));
            
            Console.WriteLine("multiplicitum 6 : " + multiplicitum( new int[] {16,28}) );
            Console.WriteLine("multiplicitum 1 : " + multiplicitum(new int[] { 1111111 }));
            Console.WriteLine("multiplicitum 2 : " + multiplicitum(new int[] { 1, 2, 3, 4, 5, 6 }));
            Console.WriteLine("multiplicitum 6: " + multiplicitum(new int[] { 8, 16, 89, 3 }));
            Console.WriteLine("multiplicitum 6: " + multiplicitum(new int[] { 26, 497, 62, 841 }));
            Console.WriteLine("multiplicitum 6: " + multiplicitum(new int[] { 17737, 98723, 2 }));
            Console.WriteLine("multiplicitum 8: " + multiplicitum(new int[] { 123, -99 }));
            Console.WriteLine("multiplicitum 8: " + multiplicitum(new int[] { 167, 167, 167, 167, 167,3 }));
            Console.WriteLine("multiplicitum 2: " + multiplicitum(new int[] { 98526, 54, 863, 156489,45, 6156}));
        }
        static int multiplicitum(int [] arr)
        {
            int num = 0;
            foreach (int el in arr)
            {
                num += el;
            }
            
            while (num >= 10)
            {
                string stringNum = num.ToString();
                int temp = 1;
                for(int i = 0; i < stringNum.Length; i++)
                {
                    temp *= Int32.Parse("" + stringNum[i]); 
                }
                num = temp;

            }
            return num;
        }
        /*static int multiplicitum(int numOne, int numTwo)
        {
            int num = numOne + numTwo;
            while(num >= 10)
            {
                string stringNum = num.ToString();
                num = Int32.Parse(""+stringNum[0])* Int32.Parse("" + stringNum[1]); ;//(int)stringNum[0] * (int)stringNum[1];
            }
            return num;
        }*/
    }
}
