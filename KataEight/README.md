

## KATA: Reducto multiplicitum
Create a function that takes numbers as arguments, adds them 
together, and returns the product of digits until the answer is only
1 digit long.
# Example
SumDigProd(16, 28) ➞ 6
16 + 28 = 44
4 * 4 =  16
1 * 6 = 6
# Notes
The input of the function is at least one number but can be any 
amount of numbers (Params).
Tests
- Assert.Equals(0, Reducer.SumDigProd(0));
- Assert.Equals(9, Reducer.SumDigProd (9));
- Assert.Equals(7, Reducer.SumDigProd (9, 8));
- Assert.Equals(6, Reducer.SumDigProd (16, 28));
- Assert.Equals(1, Reducer.SumDigProd (111111111));
- Assert.Equals(2, Reducer.SumDigProd (1, 2, 3, 4, 5, 6));
- Assert.Equals(6, Reducer.SumDigProd (8, 16, 89, 3));
- Assert.Equals(6, Reducer.SumDigProd (26, 497, 62, 841));
- Assert.Equals(6, Reducer.SumDigProd (17737, 98723, 2));
- Assert.Equals(8, Reducer.SumDigProd (123, -99));
- Assert.Equals(8, Reducer.SumDigProd (167, 167, 167, 167, 167, 
3));
- Assert.Equals(2, Reducer.SumDigProd (98526, 54, 863, 156489, 
45, 6156));
